import sys
import matplotlib.pyplot as plt
import pdb

def info(type, value, tb):
    pdb.pm()
sys.excepthook = info


#1. program arguments
# -- names of files and names in the legend
#
#2. read all things into lists ( a function to read it ... )
#
#3. prepare a plot
#
#4. save a plot
#

files = filter(lambda x: 'sca-simulator' not in x, sys.argv[1].split(','))
labels = filter(lambda x: 'sca-simulator' not in x, sys.argv[2].split(','))

def read_lines_as_floats(fn):
    f=open(fn,'r')
    return map( lambda x: 1E6*float(x), f.readlines() )



big_t = []

for fn in files:
    try:
        big_t.append(read_lines_as_floats( fn ))
    except:
        big_t.append(None)

max_r = 100
# find biggest range of all series
#for r in big_t:
#    max_r = max(max_r, max(r) )

#draw
num_samples = 0
plt.figure()
for i in range(0, len(big_t)):
    plt.hist( big_t[i], bins=100, range=(0,max_r,), alpha=0.4, label=labels[i], normed=1,lw=0 )
    num_samples += len(big_t[i])

plt.xlabel('Transport latency [us]')
plt.legend()
plt.grid()
plt.title('Distribution of transport latency(total samples='+str(num_samples)+')')
plt.savefig(sys.argv[3], dpi=150)
#plt.show()

