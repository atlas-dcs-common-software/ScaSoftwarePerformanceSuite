import glob

def add_to_bucket(bucket, item):
    if item not in bucket:
        bucket.append(item)

allocated_mt_colors = {}        
def alloc_mt_color(mt):
    global allocated_mt_colors
    mt_colors=['#eeffff','#ffeeff','#ffffee','#eeeeff']
    if mt not in allocated_mt_colors:
        allocated_mt_colors[mt] = mt_colors[ len(allocated_mt_colors) ]
    return allocated_mt_colors[mt]
        
def create_html(scenarios):
    sca_number_selection = []
    mt_selection = []
    backend_selection = []
    for scenario in scenarios:
        add_to_bucket( sca_number_selection, scenario.number_scas )
        add_to_bucket( mt_selection, scenario.mt_number )
        backend = scenario.backend
        if scenario.sim_is_remote:
            backend += '(remote)'
        add_to_bucket( backend_selection, backend )
    
    fo = file('out.html','w')
    fo.write('<html><body>')
    fo.write('Total system throughput, all values are in kHz!<br>')
    fo.write('<table border="1">')
    fo.write('<tr>')  # 1st row, headers
    fo.write('<td>#SCAs</td>')
    for b in backend_selection:
        fo.write('<td colspan='+str(len(mt_selection))+'><b>'+b+'</b></td>')
    fo.write('</tr>\n')

    fo.write('<tr>')  # 2nd row, 2nd headers with MT information
    fo.write('<td></td>')
    for b in backend_selection:
        for mt in mt_selection:
            col = alloc_mt_color(mt)
            fo.write('<td bgcolor='+col+'>mt='+str(mt)+'</td>')
    fo.write('</tr>\n')

    for sca_number in sca_number_selection:
        fo.write('<tr>')
        fo.write('<td>'+str(sca_number))
        #  all histograms for this?
        list_histo = glob.glob('results/histo_n'+str(sca_number)+'_*.png')
        for h in list_histo:
            fo.write('<br><a href="'+h+'">'+h+'</a>')
        fo.write('</td>')
        for b in backend_selection:
            for mt in mt_selection:
                col = alloc_mt_color(mt)
                fo.write('<td bgcolor='+col+'>')
                # do we have a scenario matching this result?
                remote = 'remote' in b
                scenario = filter(lambda x: x.backend == b.replace('(remote)','') and x.mt_number == mt and x.number_scas == sca_number and x.sim_is_remote == remote, scenarios)
                if len(scenario) == 1:
                    scenario = scenario[0]
                    if scenario.results['thru']=="?":
                        thru = "?"
                    else:
                        thru = str(round(scenario.results['thru']/1000,1))
                    fo.write( thru )
                    fo.write('<br><a href='+scenario.chart_path+'>chart</a>')
                fo.write('</td>')
        fo.write('</tr>')
    fo.write('</table>')

    fo.write('<img src=results/summary128.png width="50%"  >\n')
    fo.write('<img src=results/summary.png width="50%"  >')
    
                    
        
    


