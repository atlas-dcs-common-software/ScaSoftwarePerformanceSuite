from ROOT import TH1D,TCanvas,TAttFill,gStyle,TPaveText
import time
import sys
import pdb
import numpy

files = filter(lambda x: 'sca-simulator' not in x, sys.argv[1].split(','))
labels = filter(lambda x: 'sca-simulator' not in x, sys.argv[2].split(','))


i=0
T=[]

gStyle.SetOptStat(0)

c1 = TCanvas( 'c1', 'Histogram', 200, 10, 1200, 900 )
pt = TPaveText(300,60,500.0, 180)
pt.AddText("Means")
#c1.SetLogy()

max_y = 0

my_colors=[49, 46, 40, 38, 36, 30, 8, 4]

max_x = 0

data = {}
for fn in files:
    f = file(fn, 'r')
    data[fn] = map(lambda x: float(x)*1E6, f.readlines() )
    av = numpy.mean(data[fn])
    std = numpy.std(data[fn])
    max_x = max(max_x, av+2*std)
    

for j in range(0,len(files)):
    lines = data[files[j]]
    print 'Have '+str(len(lines))+' samples'
    t=TH1D(labels[j],labels[j],50,0,max_x)
    
    t.GetXaxis().SetTitle("latency [#mus]")
    t.GetYaxis().SetTitle("count")

    t.SetLineColor(my_colors[i])
    t.SetLineWidth(4)
    
    t.SetFillStyle( 3002 );
    t.SetFillColorAlpha(my_colors[i], 0.2)
    
    for x in lines:
        t.Fill(x)

    t.Scale(1.0/t.Integral())
    if i==0:
        t.Draw()
    else:
        t.Draw('same')
    T.append(t)

    max_y = max (max_y, t.GetMaximum() )
    text=pt.AddText(labels[j]+" "+str(round(t.GetMean())))
    text.SetTextAlign(11);
    text.SetTextColor(my_colors[i])
    i += 1
    
for t in T:
    
    t.SetMaximum(max_y)


#T[0].SetTitle("Distribution of transport latency")
pt.SetY1(0.4*max_y)
pt.SetY2(0.6*max_y)
pt.SetX1(0.7*max_x)
pt.SetX2(1.0*max_x)
pt.Draw()
    
legend=c1.BuildLegend()
legend.SetX1(0.2)
legend.SetTextSize(0.03)

c1.SetTitle("dupa")
c1.Update()
c1.SaveAs(sys.argv[3]+".png")


