import time
import subprocess
import os
import utils
import matplotlib.pyplot as plt
import html_report
import threading
import copy
import random
import pdb

sca_software_build_directory = "/home/pnikiel/gitProjects/ScaSoftware/build/Demonstrators/"

backend_situations=['local/sca-simulator','local/uds','local/simple-netio','remote/simple-netio']

path_of_simulator={
    'sca-simulator': None,
    'uds'          : sca_software_build_directory+'SimulatorOverUds/simulator_uds',
    'simple-netio' : sca_software_build_directory+'SimulatorOverNetIo/simulator_netio'
    }
path_of_client=sca_software_build_directory+'1000ScaAdcSimulationMultiTrans/1000sca_adc_mt'


sca_numbers=[1,8,16,32,48,64,96,128,192,256,384,512,640,768,896,1024,1536,2048]


multi_transactions=[1,4,16]   # 'half' also supported

number_iterations=100000

path_to_event_analyzer = '/home/pnikiel/gitProjects/EventAnalyzer/analyze.py'
sim_remote_machine = '10.0.0.2'

def get_mt_number(sca_number, mt):
    if mt=='half':
        return sca_number/2
    elif mt=='max':
        return sca_number
    else:
        return int(mt)

class Scenario(dict):
    def __init__(self,number_scas,backend,mt_number,sim_is_remote):
        self.number_scas = number_scas
        self.backend = backend
        self.mt_number = mt_number
        self.sim_is_remote = sim_is_remote
        self.results = {}
        self.sim_process = None
        self.client_events_path = self.get_path('client-events.xml')
        self.sim_events_path = self.get_path('sim-events.xml')
        self.chart_path = self.get_path('chart.svg')
        self.time_to_sca_path = self.get_path('to_sca.txt')
        self.time_from_sca_path = self.get_path('from_sca.txt')

        
    def short_name(self):
        return 'n'+str(self.number_scas)+'_b'+str(self.backend)+'_m'+str(self.mt_number)+('_remote' if self.sim_is_remote else '')

    def get_path(self,suffix):
        return 'results/'+self.short_name()+'-'+suffix

    def start_simulator(self):
        if self.backend == 'sca-simulator':
            return
        args = []
        if self.sim_is_remote:
            args += ['ssh', sim_remote_machine]
        args += [path_of_simulator[self.backend],'-n',str(self.number_scas),'--events_to', self.sim_events_path]
        print 'Will run : '+(' '.join(args))
        self.sim_process = subprocess.Popen(args)
        time.sleep(5 + self.number_scas/100)

    def stop_simulator(self):
        if self.backend == 'sca-simulator':
            return
        if self.sim_is_remote:
            name_to_kill = 'simulator_uds' if self.backend=='uds' else 'simulator_netio'
            os.system('ssh '+sim_remote_machine+' ./killall_check.sh '+name_to_kill) 
            time.sleep(5)
            print 'Copying remote output file to local...'
            rv = os.system('scp '+sim_remote_machine+':'+self.sim_events_path+' '+self.sim_events_path)
            print 'Result was: '+str(rv)
            print 'Now deleting on remote'
            os.system('ssh '+sim_remote_machine+' rm -v '+self.sim_events_path) 
            
        self.sim_process.send_signal(2)  # SIGINT, TODO put literal
        self.sim_process.wait()

    def run(self):
        if self.sim_is_remote:
            if os.path.isfile(self.sim_events_path) and os.path.isfile(self.client_events_path):
                print 'Unless forced, skipping this scenario'
                return
        else:
            if os.path.isfile(self.client_events_path):
                print 'Unless forced, skipping this scenario'
                return
                
        self.start_simulator()
        args = [path_of_client,'-n',str(self.number_scas),'-m',str(self.mt_number),'-t','INF','-b',self.backend,'-i','0.8s','--events_to',self.client_events_path,'-f','0','--event_recording=wait 0.1s,turn_on,wait 0.3s,turn_off']
        if self.sim_is_remote:
            args += ['--remote_addr',sim_remote_machine]
        client_process = subprocess.Popen(args)
        client_process.wait()
        self.stop_simulator()

    def analyze_events(self):
        ''' Analyzer recomputes from events file: the chart and the text files of transport durations 
            It makes sense to run it only in case either of event files is newer than the output files. '''
        analyzer_inputs = [self.client_events_path]
        if self.backend != 'sca-simulator':
            analyzer_inputs += [self.sim_events_path]
        latest_input_mtime = 0
        for inp in analyzer_inputs:
            try:
                s = os.stat( inp )
            except:
                print 'Cant open one of inputs: '+inp+', bailing out'
                self.results['dur'] = "?"
                self.results['thru'] = "?"
                return 
            latest_input_mtime = max( latest_input_mtime, s.st_mtime )

        analyzer_outputs = [self.chart_path, self.time_to_sca_path, self.time_from_sca_path]
        will_need_to_recompute = False
        for outp in analyzer_outputs:
            try:
                s = os.stat( outp )
                if s.st_mtime < latest_input_mtime:
                    will_need_to_recompute = True
                    break
            except:
                # output doesnt exist, we obligatorily need to recompute this
                will_need_to_recompute = True
                break

        print 'Will need to recompute: '+str(will_need_to_recompute)
        try:  # might fail if analyzer didnt manage to get proper data
            if will_need_to_recompute:
                print 'Necessary to recompute this scenario: '+self.short_name()
                analyzer_args = '--output_chart='+self.chart_path+' ' +self.client_events_path;
                analyzer_args += ' --output_transport_time_to_SCA '+self.time_to_sca_path
                analyzer_args += ' --output_transport_time_from_SCA '+self.time_from_sca_path
                if self.backend != 'sca-simulator':
                    analyzer_args += ' '+self.sim_events_path
                os.system('python '+path_to_event_analyzer+' '+analyzer_args)
            average_duration = utils.extract_duration( self.client_events_path )
            #  in this time we finish 'mt_number' transactions, so the total throughput of ADC transactions ON SEPARATE SCAs is
            transaction_speed = 1000000.0 / average_duration * self.number_scas
        except Exception as e:
            print 'Didnt manage to recompute because of exception: '+str(e)
            average_duration = "?"
            transaction_speed = "?"
        self.results['dur'] = average_duration
        self.results['thru'] = transaction_speed

def generate_scenarios():
    scenarios = []
    for sca_number in sca_numbers:
        for backend_situation in backend_situations:
            [sim_place,backend] = backend_situation.split('/')
            for mt in multi_transactions:
                
                mt_number = get_mt_number(sca_number, mt)
                if mt_number > sca_number:
                    continue
                scenario = Scenario(sca_number, backend, mt_number, sim_place == 'remote')
                # check duplicate
                x = filter( lambda x: x.short_name() == scenario.short_name(), scenarios)
                if len(x) < 1:
                    scenarios.append(scenario)
    return scenarios
    

    
            
# create_html( summary_sca_number )

def run_combinator(scenarios):
    ''' Combinator takes all scenarios with same number of SCAs and same MT(multitransaction) setting and puts their transport latency in a histogram'''
    scaNumberAndMtToScenariosMap = {}    
    for scenario in scenarios:
        key = (scenario.number_scas, scenario.mt_number)
        if scaNumberAndMtToScenariosMap.has_key(key):
            scaNumberAndMtToScenariosMap[key] += [scenario]
        else:
            scaNumberAndMtToScenariosMap[key] = [scenario]

    for case in scaNumberAndMtToScenariosMap:
        combinator_files = ''
        labels = ''
        for scenario in scaNumberAndMtToScenariosMap[case]:
            combinator_files += scenario.get_path( 'to_sca.txt')+','
            labels += str(scenario.number_scas)+"SCA-"+scenario.backend+('\(remote\)' if scenario.sim_is_remote else '')+"-MT"+str(scenario.mt_number)+"-toSCA,"
            combinator_files += scenario.get_path( 'from_sca.txt')+','
            labels += str(scenario.number_scas)+"SCA-"+scenario.backend+('\(remote\)' if scenario.sim_is_remote else '')+"-MT"+str(scenario.mt_number)+"-fromSCA,"
            
        combinator_files = combinator_files.rstrip(',')
        labels = labels.rstrip(',')
        histo_path = 'results/histo_n'+str(case[0])+'_m'+str(case[1])
        os.system('python combinator-root.py '+combinator_files+' '+labels+' '+histo_path)
        

used_mts = {}
def get_point_style_mt(mt):
    point_styles = ['x','o','^']
    if not mt in used_mts:
        used_mts[ mt ] = point_styles[ len(used_mts) ]
    return used_mts[mt]    
        

def make_summary_plot(scenarios,out='results/summary.png',title_supplement=''):
    point_colors = {'sca-simulator':'k', 'uds':'r', 'simple-netio':'g', 'simple-netio(remote)':'b'}
    mtBackendToScenariosMap = {}

    for scenario in scenarios:
        backend = scenario.backend
        if scenario.sim_is_remote:
            backend += '(remote)'
        key = (scenario.mt_number, backend)
        if scenario.results['thru'] != "?":
            if mtBackendToScenariosMap.has_key(key):
                mtBackendToScenariosMap[key] += [scenario]
            else:
                mtBackendToScenariosMap[key] = [scenario]
    for key in mtBackendToScenariosMap:
        x = map( lambda x: x.number_scas, mtBackendToScenariosMap[key] )
        y = map( lambda x: x.results['thru']/1000.0, mtBackendToScenariosMap[key] )  # divide by 1000 to get rate in kHz
        plt.plot(x, y, point_colors[key[1]]+get_point_style_mt(key[0])+'--', label=key[1]+' MT '+str(key[0]) )
    plt.title('Summary plot of system throughput'+title_supplement)
    plt.xlabel('# SCAs')
    plt.ylabel('Total ADC conversion rate [kHz]')
    plt.grid()
    plt.legend(loc='upper left')
    plt.savefig(out, dpi=150)
    plt.clf()
            

def worker_analyzer( scenarios ):
    for scenario in scenarios:
        print 'Now attempting analyzing: '+scenario.short_name()
        scenario.analyze_events()
    

def fair_split( N, m):
    ''' Splits N work items among m workers such that the differences among them are minimal. Returns number of items per worker.'''
    result = [N/m]*m
    for i in range(0, N%m):
        result[i] += 1
    return result
    
    
def spawn_analyzer( scenarios ):
    threads = []
    scenarios_copy = copy.copy(scenarios)
    num_jobs = 8 
    work_split = fair_split( len(scenarios), num_jobs )
    for job_no in range(0,num_jobs):
        # pick by random enough number of scenarios
        scenarios_for_worker = []
        for i in range(0, work_split[ job_no ] ):
            scenarios_for_worker.append( scenarios_copy.pop( random.randrange(0, len(scenarios_copy) ) ) )
        print 'Spawning a thread with '+str(len(scenarios_for_worker))+' work items'
        t = threading.Thread( target=worker_analyzer, args=(scenarios_for_worker,) )
        t.start()
        threads.append(t)

    for t in threads:
        t.join()
    print 'All analyzer threads finished'
        
                
            
        
    
                
def main():
    scenarios = generate_scenarios()
    for scenario in scenarios:
        print scenario.short_name()
    for scenario in scenarios:
       scenario.run()
    spawn_analyzer( scenarios )
    run_combinator(scenarios)
    make_summary_plot(scenarios)
    up_to_128SCAs = filter(lambda x: x.number_scas<=128, scenarios)

    make_summary_plot(up_to_128SCAs, 'results/summary128.png', ' zoom-in to 128SCAs' )
    html_report.create_html(scenarios)
    

main()
