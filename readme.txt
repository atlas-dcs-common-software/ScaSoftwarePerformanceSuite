SCA SW Performance Suite
Piotr Nikiel Jan-Feb 2017

Piotr's notes on how to run it

1. If you want to include remote netio in the tests, put remote's machine host name in 'sim_remote_machine', and
make sure that respective simulator is deployed at the remote machine at the same path as at local machine

2. You need the following dependencies
a) all Python packages required
b) SCA SW suite compiled with respective options (i.e. if you want to benchmark netio backend, you need netio support in SCA SW)
c) EventAnalyzer (poke Piotr for address)

3. Synchronizing time for remote situation

The only sane way to gather events from multiple machines and analyze them is to first synchronize the realtime clocks of both machines.
Thanks to Tomasz Wlostowski, I know one needs Precision Time Protocol to do it.
Here is how I managed to do this:
- on my desktop machine, I've run ptp:

sudo ptp4l -m -i enp3s0 -S

- on my laptop machime, I've run ptp in slave only mode:

sudo ptp4l -l 5  -H -i em1 -m -s

and in the second window, the phc2sys (because my laptop NIC had hardware clock soource; the following might not be necessary if you only have software timestamping):

sudo phc2sys  -a -r   -m -P 0.2 -I 0.05

- the constants above I chosen as lower regulation coefficients compared to default values which are 10-20 bigger. The reason is that when I had big traffic on the cable,
the clock diverged to +- 100us with default settings; with those coefficients it diverged to +- 5us
